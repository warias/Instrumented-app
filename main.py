from flask import Flask, render_template
from prometheus_client import Counter, start_http_server

# Application instance
app = Flask(__name__)

requests = Counter('study_programs_calls', 'Study Programs Calls')
start_http_server(8000)

@app.route('/')
def render_index():
    return render_template('index.html')


@app.route('/programs')
def programs():
    requests.inc()
    return render_template('programs.html')


@app.route('/debug/<string>')
def print_hi(string):
    return f'Hi, {string}'


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    app.run()



## Python Instrumented app example

This simple script illustrates how to use Prometheus Client to add monitoring to an endpoint,  plotting its response into the Out Of The Box dashboards provided by Gitlab Metrics.

Goal: Instrument the application using *Prometheus Python Client* and monitor one of the application endpoints in Gitlab Metrics

*The steps below assume this sample web application has been containerized, and it's ready to be deployed to Kubernetes Cluster.  

1. We will use this simple web application and **add** monitoring to it.

```
app = Flask(__name__)

@app.route('/')
def render_index():
    return render_template('index.html')

@app.route('/programs')
def programs():
    return render_template('programs.html')
``` 


2. The application above serves a site that looks like this:  

![index](screens/index.png)
   

3. Let's assume that we are interested in finding out how many people are visiting the *Study Programs* page  
To do that, we will monitor the `/programs` endpoint

4. Install and add the required imports:  
`from prometheus_client import Counter, start_http_server`
   
5. Initialize the *Counter* with its respective metric name and description:  
`requests = Counter('study_programs_calls', 'Study Programs Calls')`
   
6. Start the http server where we will send the metrics in order to get collected by  Prometheus server  
`start_http_server(8000)`
   
7. Given that we want to monitor the `/programs` endpoint, initialize the *Counter* within the decorated endpoint function  
`requests.inc()`

8. The final code should look something like this:  

```
app = Flask(__name__)

requests = Counter('study_programs_calls', 'Study Programs Calls')
start_http_server(8000)

@app.route('/')
def render_index():
    return render_template('index.html')


@app.route('/programs')
def programs():
    requests.inc()
    return render_template('programs.html')
    
if __name__ == '__main__':
    app.run()
``` 

9. Deploy your application to your Kubernetes Cluster with [Gitlab CI]  
10. Click on 
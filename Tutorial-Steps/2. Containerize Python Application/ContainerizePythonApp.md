## Containerize Python Application Using Gitlab DevOps Platform 

Gitlab DevOps Platform allows you to have complete end to end visibility of your Software Development Life Cycle.  
We will cover in a series of blog/video posts different capabilities from Gitlab, each one with a specific focus. Most of the samples used in this series are built from scratch, so you can potentially customize them for your needs.  

**This first part covers how to take advantage of Gitlab CI to containerize our applications.**

*Developing* a project locally and getting it to run in a container require us to pack our code as a Docker image.
Building an image everytime we change something in our code involves some manual steps that can be automated using Gitlab CI.

Let's learn how we can containerize our app using Gitlab DevOps Platform CI!

**Requirements**

- Gitlab DevOps Platform
- Python Flask web application. You can find the example used in this walk-though [here](https://gitlab.com/warias/Instrumented-app)

Let's get started!

1. Let's create a file called `Dockerfile`. This file will contain the instructions Gitlab CI will utilize to build an image.

2. In the `Dockerfile` we will add the following instructions:  

````
FROM python:3.7

COPY main.py /app/main.py
COPY static /app/static/
COPY templates /app/templates
COPY requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["main.py"]

````
The Dockerfile above was created for our sample web application, you can adapt this to fit your own project schema

4. Now, let's push our newly created Dockerfile to our Gitlab project:  

![Dockerfile](../images/DockerFileInRepo.png)

5. With the Dockerfile in our repo, let's proceed to create our **.gitlab-ci.yml file**. This will enable a CI pipeline, and will be in charge of building a new docker image
every time we commit a change to our application code.  
   Our Gitlab CI file for this example looks like this: 
   
``` 
default:
    image: python:3.7

variables:
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""
    DOCKER_HOST: tcp://docker:2375
    MOUNT_POINT: /builds/$CI_PROJECT_PATH/mnt
    CONTAINER_IMAGE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA

services:
    - docker:dind

stages: 
    - build
    
build-flask-image:
    stage: build
    image: docker:19.03.1
    script:
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
        - docker build -t $CONTAINER_IMAGE -f Dockerfile .
        - docker push $CONTAINER_IMAGE

    tags:
        - docker


```
6. Great! committing our newly created `.gitlab-ci.yml` file to our project will automatically start the pipeline executing the steps we defined above.  

![pipeline](../images/Pipeline.png)

7. This `build` step in our pipeline will:  
   - Clone the repository containing our Python application code and Dockerfile 
   - Execute the docker build command 
   - Name and tag the newly created image using the variables defined in the .gitlab-ci.yml file
   - Push the new Docker image to Gitlab Registry
   And all of this happens automatically every time we commit a change to our code. Saving us time and making it less prone to errors.  
     
8. Lastly, you can check the image was automatically created and pushed to Gitlab Registry by navigating to *Packages & Registries*  
![registry](../images/Registry.png)
   

### What's next? 

This post showed how to take our code and containerize it. Gitlab DevOps platform enable us to quickly run
through the steps required to achieve containerization preparing our applications to be deployed in Cloud Native infrastructures such as Kubernetes clusters.
In the next post of this series, we will integrate Gitlab DevOps Platform with a Kubernetes Cluster and deploy our Containerized flask application to a Pod.




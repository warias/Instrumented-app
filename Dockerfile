FROM python:3.7

COPY main.py /app/main.py
COPY static /app/static/
COPY templates /app/templates
COPY requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["main.py"]
